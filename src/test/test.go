package main

import (
	"flag"
	"fmt"
	"os"
	//	"encoding/xml"
	//	"github.com/clbanning/mxj"
	"github.com/go-stomp/stomp"
	//	"gopkg.in/mgo.v2"
)

var data = `<?xml version="1.0" encoding="UTF-8"?>
<d2LogicalModel modelBaseVersion="2" xmlns="http://datex2.eu/schema/2/2_0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://datex2.eu/schema/2/2_0 http://www.datex2.eu/schema/2/2_2/DATEXIISchema_2_2_2.xsd">
	<exchange>
		<supplierIdentification>
			<country>fr</country>
			<nationalIdentifier>X</nationalIdentifier>
		</supplierIdentification>
	</exchange>
	<payloadPublication xsi:type="ElaboratedDataPublication" lang="fr">
		<publicationTime>2006-11-09T15:52:00+01:00</publicationTime>
		<publicationCreator>
			<country>fr</country>
			<nationalIdentifier>X2</nationalIdentifier>
		</publicationCreator>
		<headerInformation>
			<confidentiality>noRestriction</confidentiality>
			<informationStatus>real</informationStatus>
		</headerInformation>
		<elaboratedData>
			<basicData xsi:type="TravelTimeData">
				<measurementOrCalculationTime>2010-01-02T15:32:00+02:00</measurementOrCalculationTime>
				<pertinentLocation xsi:type="Linear">
					<alertCLinear xsi:type="AlertCMethod2Linear">
						<alertCLocationCountryCode>14</alertCLocationCountryCode>
						<alertCLocationTableNumber>30</alertCLocationTableNumber>
						<alertCLocationTableVersion>1</alertCLocationTableVersion>
						<alertCDirection>
							<alertCDirectionCoded>positive</alertCDirectionCoded>
						</alertCDirection>
						<alertCMethod2PrimaryPointLocation>
							<alertCLocation>
								<specificLocation>1243</specificLocation>
							</alertCLocation>
						</alertCMethod2PrimaryPointLocation>
						<alertCMethod2SecondaryPointLocation>
							<alertCLocation>
								<specificLocation>1244</specificLocation>
							</alertCLocation>
						</alertCMethod2SecondaryPointLocation>
					</alertCLinear>
				</pertinentLocation>
				<travelTimeTrendType>increasing</travelTimeTrendType>
				<travelTime>
					<duration>271</duration>
				</travelTime>
				<freeFlowTravelTime>
					<duration>250</duration>
				</freeFlowTravelTime>
				<freeFlowSpeed>
					<speed>72</speed>
				</freeFlowSpeed>
			</basicData>
		</elaboratedData>
		<elaboratedData>
			<basicData xsi:type="TravelTimeData">
				<pertinentLocation xsi:type="LocationByReference">
					<predefinedLocationReference targetClass="PredefinedLocation" id="GUID1234277721992" version="0"/>
				</pertinentLocation>
				<travelTimeTrendType>increasing</travelTimeTrendType>
				<travelTime>
					<duration>271</duration>
				</travelTime>
				<freeFlowTravelTime>
					<duration>250</duration>
				</freeFlowTravelTime>
				<freeFlowSpeed>
					<speed>72</speed>
				</freeFlowSpeed>
			</basicData>
		</elaboratedData>
	</payloadPublication>
</d2LogicalModel>`

var data2 = `<payloadPublication xsi:type="ElaboratedDataPublication" lang="fr">
<publicationTime>2006-11-09T15:52:00+01:00</publicationTime>
<publicationCreator>
<country>fr</country>
<nationalIdentifier>X2</nationalIdentifier>
</publicationCreator>
<headerInformation>
<confidentiality>noRestriction</confidentiality>
<informationStatus>real</informationStatus>
</headerInformation>
<elaboratedData>
<basicData xsi:type="TravelTimeData">
<pertinentLocation xsi:type="LocationByReference">
<predefinedLocationReference targetClass="PredefinedLocation" id="GUID1234277721992" version="0"/>
</pertinentLocation>
<travelTimeTrendType>increasing</travelTimeTrendType>
<travelTime>
<duration>271</duration>
</travelTime>
<freeFlowTravelTime>
<duration>250</duration>
</freeFlowTravelTime>
<freeFlowSpeed>
<speed>72</speed>
</freeFlowSpeed>
</basicData>
</elaboratedData>
</payloadPublication>`

//
//func main() {
//	m, _ := mxj.NewMapXml([]byte(data)) // unmarshal
//
//	result, _ := m.JsonIndent("", "    ")
//
//	fmt.Println(string(result))
//
//}

const defaultPort = ":61613"

var serverAddr = flag.String("server", "10.88.36.31:61613", "STOMP server endpoint")
var messageCount = flag.Int("count", 10, "Number of messages to send/receive")
var queueName = flag.String("queue", "/queue/client_test", "Destination queue")
var helpFlag = flag.Bool("help", false, "Print help text")
var stop = make(chan bool)

// these are the default options that work with RabbitMQ
var options = &stomp.Conn{
//	Login:    "karaf",
//	Passcode: "karaf",
//	Host:     "/",
}

func main() {
	flag.Parse()
	if *helpFlag {
		fmt.Fprintf(os.Stderr, "Usage of %s\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	subscribed := make(chan bool)
	go recvMessages(subscribed)

	// wait until we know the receiver has subscribed
	<-subscribed

	go sendMessages()

	<-stop
	<-stop
}

func sendMessages() {
	defer func() {
		stop <- true
	}()

	conn, err := stomp.Dial("tcp", *serverAddr, nil)
	if err != nil {
		println("cannot connect to server", err.Error())
		return
	}

	for i := 1; i <= *messageCount; i++ {
		text := fmt.Sprintf("Message #%d", i)
		err = conn.Send(*queueName, "text/plain",
			[]byte(text), nil)
		if err != nil {
			println("failed to send to server", err)
			return
		}
	}
	println("sender finished")
}

func recvMessages(subscribed chan bool) {
	defer func() {
		stop <- true
	}()

	conn, err := stomp.Dial("tcp", *serverAddr, nil)
	if err != nil {
		println("cannot connect to server", err.Error())
		return
	}

	sub, err := conn.Subscribe(*queueName, stomp.AckAuto)
	if err != nil {
		println("cannot subscribe to", *queueName, err.Error())
		return
	}
	close(subscribed)

	for i := 1; i <= *messageCount; i++ {
		msg := <-sub.C
		expectedText := fmt.Sprintf("Message #%d", i)
		actualText := string(msg.Body)
		if expectedText != actualText {
			println("Expected:", expectedText)
			println("Actual:", actualText)
		}
	}
	println("receiver finished")

}
