// Simple Mysql & Postgres connection

package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"log"
)

type Way struct {
	geom              string
	gid               int32
	class_id          int32
	length            float32
	name              string
	x1                float32
	y1                float32
	x2                float32
	y2                float32
	reverse_cost      float64
	rule              sql.NullString
	to_cost           sql.NullFloat64
	maxspeed_forward  int32
	maxspeed_backward int32
	osm_id            int64
	priority          int16
	the_geom          string
	source            int32
	target            int32
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
		panic(e)
	}
}

func main() {
	log.Println("Start")

	postgresDb, err := sql.Open("postgres", "postgres://routing:@10.88.224.132:5433/hcm-routing")
	//postgresDb, err := sql.Open("postgres", "postgres://maprenderer:@10.88.224.132:5432/routing")
	check(err)

	rows, err := postgresDb.Query("SELECT ST_AsText(ST_RemoveRepeatedPoints(the_geom)), * FROM ways where length > 0")
	check(err)
	defer rows.Close()

	mysqlDb, err := sql.Open("mysql", "root@tcp(10.88.224.21:3306)/bus_log")
	check(err)
	defer mysqlDb.Close()

	stmtIns, err := mysqlDb.Prepare("INSERT INTO ways VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )")
	check(err)
	defer stmtIns.Close()

	rec := 0
	for rows.Next() {
		var way Way
		err := rows.Scan(&way.geom, &way.gid, &way.class_id, &way.length, &way.name, &way.x1, &way.y1, &way.x2, &way.y2, &way.reverse_cost, &way.rule, &way.to_cost, &way.maxspeed_forward, &way.maxspeed_backward, &way.osm_id, &way.priority, &way.the_geom, &way.source, &way.target)
		check(err)

		_, err = stmtIns.Exec(way.gid, way.class_id, way.length, way.name, way.x1, way.y1, way.x2, way.y2, way.reverse_cost, way.rule, way.to_cost, way.maxspeed_forward, way.maxspeed_backward, way.osm_id, way.priority, way.geom, way.source, way.target)
		check(err)

		rec++
	}

	log.Println("Total records: ", rec)
	log.Println("End")
}
