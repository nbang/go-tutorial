package main

import (
	"runtime"
	"time"
)

var a, b, c int

func u1() {
	a, b, c = 1, 2, 3
}

func u2() {
	a, b, c = 4, 5, 6
}

func p() {
	println(a, b, c)
}

func main() {
	go u1()
	go u2()
	go p()
	time.Sleep(1 * time.Second)
}

func init() {
	runtime.GOMAXPROCS(3)
}
