/*
   Implement simple datex handler
   Subcribe datex messages from mqtt and insert into mongodb
*/

package main

import (
	"fmt"
	"log"

	"github.com/clbanning/mxj"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gopkg.in/mgo.v2"
)

func check(e error) {
	if e != nil {
		log.Fatal(e.Error())
		panic(e)
	}
}

//var doc = `{"@xmlns":"http://datex2.eu/schema/2/2_0","@modelBaseVersion":"2","exchange":{"supplierIdentification":{"nationalIdentifier":"VN"}},"payloadPublication":{"@xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance","@xsi:type":"SituationPublication","publicationTime":"2015-07-29T10:41:58.131+07:00","situation":{"situationRecord":{"@xmlns:xsi":"http://www.w3.org/2001/XMLSchema-instance","@xsi:type":"AbnormalTraffic","@id":"72667f28-44a6-4200-896e-490c2c28bc49","@version":"1","situationRecordCreationTime":"2015-07-29T10:41:58.131+07:00","source":{"sourceName":{"values":{"value":"test"}}},"groupOfLocations":{"@xsi:type":"LocationByReference","predefinedLocationReference":{"@targetClass":"PredefinedLocation","@id":"123"}},"abnormalTrafficType":"heavyTraffic"}}}}`
var (
	topic  = "DATEX2"
	url    = "localhost"
	broker = "tcp://10.88.36.31:1883"
	stop   = make(chan bool)
)

func doConnect() (s *mgo.Session, c mqtt.Client) {
	s, err := mgo.Dial(url)
	check(err)

	opts := mqtt.NewClientOptions().AddBroker(broker)
	opts.SetClientID(fmt.Sprintf("Go-client-%d", time.Now().Unix()))
	opts.SetUsername("karaf")
	opts.SetPassword("karaf")
	opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {

		collection := s.DB("datex2").C("datex2")

		m, err := mxj.NewMapXml(msg.Payload()) // unmarshal
		check(err)

		err = collection.Insert(m)
		check(err)

		println(string(msg.Payload()))
	})

	c = mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	if token := c.Subscribe(topic, 1, nil); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	return s, c
}

func doDisconnect(session *mgo.Session, client mqtt.Client) {

	if token := client.Unsubscribe(topic); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	defer session.Close()
	defer client.Disconnect(0)
}

func main() {
	session, client := doConnect()

	<-stop

	doDisconnect(session, client)

}
