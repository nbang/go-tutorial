package main

import (
	"encoding/json"
	"fmt"
	"hash/crc64"
	"log"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type BusLog struct {
	Vehicle  string  `json:"vehicle"`
	Driver   string  `json:"driver"`
	Speed    float32 `json:"speed"`
	X        float64 `json:"x"`
	Y        float64 `json:"y"`
	Ignition bool    `json:"ignition"`
	Heading  int32   `json:"heading"`
	Datetime int64   `json:"datetime"`
}

func getVehicleId(lp string) int64 {
	return int64(crc64.Checksum([]byte(lp), table))
}

var stop = make(chan bool)
var table = crc64.MakeTable(crc64.ISO)

func main() {
	log.Println("Start")

	opts := mqtt.NewClientOptions().AddBroker("tcp://10.88.224.21:1883")
	opts.SetClientID(fmt.Sprintf("Go-client-%d", time.Now().Unix()))
	opts.SetUsername("karaf")
	opts.SetPassword("karaf")
	opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {
		res := BusLog{}
		json.Unmarshal(msg.Payload(), &res)

		if res.Ignition || res.Speed > 0 {
			log.Println(res)
			log.Println("Vehicle Id: ", getVehicleId(res.Driver))
		}
	})

	log.Println(opts)

	c := mqtt.NewClient(opts)

	if token := c.Connect(); token.Wait() && token.Error() != nil {
		log.Println(token.Error())
		panic(token.Error())
	}

	if token := c.Subscribe("stis/input/buslog/point", 1, nil); token.Wait() && token.Error() != nil {
		log.Println(token.Error())
		os.Exit(1)
	}

	<-stop

	defer c.Disconnect(250)
}
