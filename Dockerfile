FROM alpine:latest

ADD ./main /usr/local/bin

ENTRYPOINT ["main"]
